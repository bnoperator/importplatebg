# Import plate background file
 
Import data using the plate background format.
 
# Create app and deploy

Clone the repo and build the package then from the package folder run the following.

```
bntools::createApp(tags=c('Import data', 'Test'), mainCategory = 'Import data')

```

```
git add -A && git commit -m "add tag Test" && git push && git tag -a 1.6 -m "++" && git push --tags
```

```
bntools::deployGitApp('https://bitbucket.org/bnoperator/importplatebg.git', '1.6')
```
  

```
bnshiny::startBNTestShiny('importplatebg')
# see workspace.R for a full example
```

# Change
## 1.0
- first version

## 1.1 , 1.2, 1.3, 1.4, 1.5
- bumped versions

## 1.6
- changed the default in the import
